# Mocean SMS Login

[MoceanSMS Login module](https://www.drupal.org/project/mocean_sms_login) provides 2FA login by using MoceanAPI service.

## Requirements

Users are required to configure a telephone field at Administration > Configuration > People > Account settings > Manage fields.

[Telephone Validation](https://www.drupal.org/project/telephone_validation) is recommended to ensure phone numbers are in correct format. After installation, it can be enable through Edit in Manage Fields.

## Mocean Account

To use the module, a Mocean account is required to configure MoceanAPI key and MoceanAPI secret. To login or sign up visit [Mocean Dashboard](https://dashboard.moceanapi.com/).