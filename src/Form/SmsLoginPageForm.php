<?php

namespace Drupal\mocean_sms_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mocean_sms_login\Utility;
use Drupal\user\Entity\User;
use Drupal\user\Entity;
use Drupal\Core\Database\Database;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

class SmsLoginPageForm extends FormBase {
 
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_login_form';
  }  
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
 
	$form['mocean_sms_login_form']['reminder'] = [
     '#type' => 'markup',
     '#markup' => '<p>Select enable and a verification code will be sent for verification, users who have successfully opted-in but changed their mobile numbers will be automatically opted-out.</p>',
	];
	
    //sends verification code to user for opt-in
    $form['mocean_sms_login_form']['enable'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enable'),
	  '#validate' => ['::enableValidation'],
    ];
	
	//user opt-out
    $form['mocean_sms_login_form']['disable'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disable'),
	  '#submit' => ['::disableSmsLogin'],
    ];
	
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  
  /**
   * {@inheritdoc}
   */
  public function enableValidation(array &$form, FormStateInterface $form_state) {
	$user = User::load(\Drupal::currentUser()->id());
	$uid= $user->get('uid')->value;
	
	$database = \Drupal::database();
	//Check if user has already opted-in and if opted-in user made changes to mobile number
    $query = $database->query('SELECT phone FROM mocean_sms_login WHERE id = :id LIMIT 1', [':id' => $uid,]);
    $result = $query->fetchField();
	
	if ($result != FALSE) {
      $phone = $user->get((new Utility)->getFieldName())->value;
	  if ($result == $phone) {
	    $form_state->setErrorByName('enable', t('You have already opted-in.'));
	  }
	}
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$user = User::load(\Drupal::currentUser()->id());
    $uid= $user->get('uid')->value;
    $phone = $user->get((new Utility)->getFieldName())->value;
	
	//Function called to send verification code
	$jsonResponse = (new Utility)->smsLoginSendCode($phone);
	
	if ($jsonResponse['status'] == 0) {
      //Verification code sent, redirect to page for entering verification code
	  $form_state->setRedirect('mocean_sms_login.sms_login_opt_in_form', ['user' => $uid, 'arg' => $jsonResponse['reqid']]);
	}
	else if (in_array($jsonResponse['status'], [16,17])) {
	  $this->messenger()->addError($this->t('Incorrect code or code expired.'));
	}
    else if ($jsonResponse['status'] == 19) {
	  $this->messenger()->addError($this->t('Operation too frequent.'));
	}
    else if (in_array($jsonResponse['status'], [1,2,18])) {
	  $this->messenger()->addError($this->t("Authorization failed or low on credit, report to the site's admin."));
	}
    else if ($jsonResponse['status'] == 15) {
      $this->messenger()->addError($this->t('Incorrect code was provided too many times. Maximum retry limit is 3.'));
	}
    else {
      $this->messenger()->addError($this->t('An error has occured, please try again later.'));
    }
  }
  
  public static function disableSmsLogin(array &$form, FormStateInterface $form_state) {
	$user = User::load(\Drupal::currentUser()->id());
	$uid= $user->get('uid')->value;
    
	$database = \Drupal::database();
	$database->delete('mocean_sms_login')
	  ->condition('id', $uid)
      ->execute();
	  
	\Drupal::messenger()->addMessage(t('You have successfully opted-out.'));
  }
}
