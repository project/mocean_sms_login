<?php

namespace Drupal\mocean_sms_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mocean_sms_login\Utility;
use Drupal\user\Entity\User;
use Drupal\user\Entity;
use Drupal\Core\Database\Database;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\Core\Routing;

class SmsLoginOptInForm extends FormBase {
	  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_login_opt_in_form';
  }  
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	
    $form['mocean_sms_login_opt_in_form']['code'] = [
      '#title' => t('Verification Code'),
      '#type' => 'textfield',
      '#size' => 6,
      '#maxlength' => 6,
      '#required' => TRUE,
    ];
	
	//Submit button
	$form['mocean_sms_login_opt_in_form']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
	
	//Resend code button
    $form['mocean_sms_login_opt_in_form']['resend'] = [
      '#type' => 'submit',
      '#value' => $this->t('Resend code'),
      '#submit' => ['::resendCode'],
    ];

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$user = User::load(\Drupal::currentUser()->id());
    $phone = $user->get((new Utility)->getFieldName())->value;
	$reqid = \Drupal::routeMatch()->getParameter('arg');
	
	//Function verify verification code
	$jsonResponse = (new Utility)->smsLoginVerifyCode($reqid, $form_state->getValue('code'));
	
	if ($jsonResponse['status'] == 0) {
	  $database = \Drupal::database();
	  $user = User::load(\Drupal::currentUser()->id());
	  $uid = $user->get('uid')->value;
      $query = $database->query('SELECT phone FROM mocean_sms_login WHERE id = :id LIMIT 1', [':id' => $uid,]);
      $result = $query->fetchField();
	  
	  if($result != FALSE) {
	    $database->update('mocean_sms_login')
          ->fields(['phone' => $phone,])
          ->condition('id', $uid, '=')
          ->execute();
	  }
	  else {
	    $database->insert('mocean_sms_login')->fields(
          array(
		    'id' => $uid,
		    'phone' => $phone,
		  )
        )->execute();
	  }
	  $form_state->setRedirect('mocean_sms_login.sms_login_form', ['user' => $uid]);
	  $this->messenger()->addStatus($this->t('You have successfully opted-in.'));
	}
	else if (in_array($jsonResponse['status'], [16,17])) {
	 $this->messenger()->addError($this->t('Incorrect code or code expired.'));
	}
    else if ($jsonResponse['status'] == 19) {
	 $this->messenger()->addError($this->t('Operation too frequent.'));
	}
    else if (in_array($jsonResponse['status'], [1,2,18])) {
	  $this->messenger()->addError($this->t("Authorization failed or low on credit, report to the site's admin."));
	}
    else if ($jsonResponse['status'] == 15) {
      $this->messenger()->addError($this->t('Incorrect code was provided too many times. Maximum retry limit is 3.'));
	}
    else {
      $this->messenger()->addError($this->t('An error has occured, please try again later.'));
    }
  }
  
  public static function resendCode(array &$form, FormStateInterface $form_state) {
    $user = User::load(\Drupal::currentUser()->id());
	$uid= $user->get('uid')->value;
    $phone = $user->get((new Utility)->getFieldName())->value;
	
	//Function send verification code
	$jsonResponse = (new Utility)->smsLoginSendCode($phone);
	
	if ($jsonResponse['status'] == 0) {
	  //Verification resent successfully, redirect to new page for entering verification code
	  $form_state->setRedirect('mocean_sms_login.sms_login_verify_form', ['user' => $uid, 'arg' => $jsonResponse['reqid']]);
	  \Drupal::messenger()->addStatus(t('Verification code resent.'));
	}
	else if (in_array($jsonResponse['status'], [16,17])) {
	  \Drupal::messenger()->addError(t('Incorrect code or code expired.'));
	}
    else if ($jsonResponse['status'] == 19) {
	  \Drupal::messenger()->addError(t('Operation too frequent.'));
	}
    else if (in_array($jsonResponse['status'], [1,2,18])) {
	  \Drupal::messenger()->addError(t("Authorization failed or low on credit, report to the site's admin."));
	}
    else if ($jsonResponse['status'] == 15) {
      \Drupal::messenger()->addError(t('Incorrect code was provided too many times. Maximum retry limit is 3.'));
	}
    else {
      \Drupal::messenger()->addError(t('An error has occured, please try again later.'));
    }
  }
}
