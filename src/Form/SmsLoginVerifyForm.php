<?php

namespace Drupal\mocean_sms_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\Entity;
use Drupal\Core\Database\Database;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\Core\Routing;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mocean_sms_login\Utility;

class SmsLoginVerifyForm extends FormBase {
	  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_login_verify_form';
  }  
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::routeMatch()->getParameter('uid');
    $database = \Drupal::database();
    $query = $database->query('SELECT verified FROM mocean_sms_login WHERE id = :id LIMIT 1', [':id' => $uid,]);
    $verified = $query->fetchField();
    
    if (\Drupal::currentUser()->isAuthenticated()) { 
      $user = \Drupal\user\Entity\User::load($uid);
      user_logout($user);	
    }

    $form['mocean_sms_login_verify_form']['code'] = [
      '#title' => t('Verification Code'),
      '#type' => 'textfield',
      '#size' => 6,
      '#maxlength' => 6,
      //'#required' => TRUE,
    ];
	
	//Submit button
	$form['mocean_sms_login_verify_form']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
	
	//Resend code button
    $form['mocean_sms_login_verify_form']['resend'] = [
      '#type' => 'submit',
      '#value' => $this->t('Resend code'),
      '#submit' => ['::resendCode'],
    ];

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::routeMatch()->getParameter('uid');
    $reqid = \Drupal::routeMatch()->getParameter('arg');
	
	//Function verify verification code
	$jsonResponse = (new Utility)->smsLoginVerifyCode($reqid, $form_state->getValue('code'));
	
	if ($jsonResponse['status'] == 0 && $this->currentUser()->isAnonymous()) {
      $user = \Drupal\user\Entity\User::load($uid);
      $database = \Drupal::database();
      $database->update('mocean_sms_login')
        ->fields(['verified' => 1,])
        ->condition('id', $uid)
        ->execute();
        
      user_login_finalize($user);
      $form_state->setRedirect('<front>');
	}
	else if (in_array($jsonResponse['status'], [16,17])) {
	 $this->messenger()->addError($this->t('Incorrect code or code expired.'));
	}
    else if ($jsonResponse['status'] == 19) {
	 $this->messenger()->addError($this->t('Operation too frequent.'));
	}
    else if (in_array($jsonResponse['status'], [1,2,18])) {
	  $this->messenger()->addError($this->t("Authorization failed or low on credit, report to the site's admin."));
	}
    else if ($jsonResponse['status'] == 15) {
      $this->messenger()->addError($this->t('Incorrect code was provided too many times. Maximum retry limit is 3.'));
	}
    else {
      $this->messenger()->addError($this->t('An error has occured, please try again later.'));
    }
  }
  
  public static function resendCode(array &$form, FormStateInterface $form_state) {
	$uid = \Drupal::routeMatch()->getParameter('uid');
    $database = \Drupal::database();
    $query = $database->query('SELECT phone FROM mocean_sms_login WHERE id = :id LIMIT 1', [':id' => $uid,]);
    $phone = $query->fetchField();
	
	//Function called to verification send code
	$jsonResponse = (new Utility)->smsLoginSendCode($phone);
	
	if ($jsonResponse['status'] == 0) {
	  //Verification resent successfully, redirect to new page for entering verification code
	  $form_state->setRedirect('mocean_sms_login.sms_login_verify_form', ['uid' => $uid, 'arg' => $jsonResponse['reqid']]);
	  \Drupal::messenger()->addStatus(t('Verification code resent.'));
	}
	else if (in_array($jsonResponse['status'], [16,17])) {
	  \Drupal::messenger()->addError(t('Incorrect code or code expired.'));
	}
    else if ($jsonResponse['status'] == 19) {
	  \Drupal::messenger()->addError(t('Operation too frequent.'));
	}
    else if (in_array($jsonResponse['status'], [1,2,18])) {
	  \Drupal::messenger()->addError(t("Authorization failed or low on credit, report to the site's admin."));
	}
    else if ($jsonResponse['status'] == 15) {
      \Drupal::messenger()->addError(t('Incorrect code was provided too many times. Maximum retry limit is 3.'));
	}
    else {
      \Drupal::messenger()->addError(t('An error has occured, please try again later.'));
    }
  }
  
}
