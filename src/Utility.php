<?php

namespace Drupal\mocean_sms_login;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

class Utility {

 /**
  * Verify API send code.
  */
  public function smsLoginSendCode($userPhone) {

    $sms_login_settings = \Drupal::config('mocean_sms_login.settings');

	$url = 'https://rest.moceanapi.com/rest/1/verify/req';
    $fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-to'=>$userPhone,
	  'mocean-brand'=>$sms_login_settings->get('message_from'),
	  'mocean-resp-format'=>'json',
    'mocean-medium'=>'drupal_2fa_login'
	);

    $fields_string = '';
	foreach($fields as $key=>$value) {
	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

	$json = json_decode($result, true);

	//returns an array
	return $json;

  }

 /**
  * Verify API verify code.
  */
  public function smsLoginVerifyCode($reqid, $code) {

	$sms_login_settings = \Drupal::config('mocean_sms_login.settings');

	$url = 'https://rest.moceanapi.com/rest/1/verify/check';
	$fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-reqid'=>$reqid,
	  'mocean-code'=>$code,
	  'mocean-resp-format'=>'json',
      'mocean-medium'=>'drupal_2fa_login'
	);

	$fields_string = '';
	foreach($fields as $key=>$value) {
  	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

	$json = json_decode($result, true);

	//returns an array
	return $json;

  }

 /**
  * Query API check credits.
  */
  public function smsLoginGetCredit() {

	$sms_login_settings = \Drupal::config('mocean_sms_login.settings');

	$url = 'https://rest.moceanapi.com/rest/1/account/balance?';
	$fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-resp-format'=>'json'
	);

	$fields_string = '';
	foreach($fields as $key=>$value) {
	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$url_final = $url.$fields_string;

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url_final);
	curl_setopt($ch,CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

    $json = json_decode($result, true);

    //returns an array
	return $json;
  }

 /**
  * Query API check pricing.
  */
  public function smsLoginGetPricing() {

	$sms_login_settings = \Drupal::config('mocean_sms_login.settings');

	$url = 'https://rest.moceanapi.com/rest/2/account/pricing?';
	$fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-resp-format'=>'json',
	  'mocean-type'=>'verify'
	);

	$fields_string = '';
	foreach($fields as $key=>$value) {
	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$url_final = $url.$fields_string;

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url_final);
	curl_setopt($ch,CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

	$json = json_decode($result, true);

	//returns an array
	return $json;
  }

 /**
  * Fetch field name for telephone.
  */
  public function getFieldName() {
    $sms_login_settings = \Drupal::config('mocean_sms_login.settings');

	return $sms_login_settings->get('field_name');
  }
}
