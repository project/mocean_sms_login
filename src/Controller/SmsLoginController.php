<?php

namespace Drupal\mocean_sms_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for mocean_sms_login pages.
 */
class SmsLoginController extends ControllerBase {

  public function smsLoginPage() {
    $build = [];
    $build['mocean_sms_login_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_login\Form\SmsLoginPageForm');
    return $build;
  }
  
  public function smsOptInPage() {
    $build = [];
    $build['mocean_sms_login_opt_in_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_login\Form\SmsLoginOptInForm');
    return $build;
  }
  
  public function smsVerifyPage() {
    $build = [];
    $build['mocean_sms_login_verify_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_login\Form\SmsLoginVerifyForm');
    return $build;
  }
}
